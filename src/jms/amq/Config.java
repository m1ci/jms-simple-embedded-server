package jms.amq;

public class Config {
    
    // Defines the JNDI context factory.
    public final static String JNDI_FACTORY = "org.apache.activemq.jndi.ActiveMQInitialContextFactory";

    // Defines the JMS context factory.
    public final static String JMS_FACTORY = "ConnectionFactory";

    // URL
    public final static String PROVIDER_URL = "tcp://localhost:61616";

}